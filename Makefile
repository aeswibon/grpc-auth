.PHONY: generate

generate: generate-messages generate-services

generate-messages:
	@echo "Generating proto code for messages..."
	@protoc --proto_path=./ --go_out=pb --go_opt=paths=source_relative --go-grpc_out=pb --go-grpc_opt=paths=source_relative proto/messages/*.proto

generate-services:
	@echo "Generating proto code for services..."
	@protoc --proto_path=./ --go_out=pb --go_opt=paths=source_relative --go-grpc_out=pb --go-grpc_opt=paths=source_relative proto/services/*.proto